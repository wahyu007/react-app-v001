import React from 'react';

const CreateUser = ({ onChangeForm, createUser, setNewNumber, data }) => {
    return(
        <div className="container">
            <div className="row">
                <div className="form-group col-md-4">
                    <label htmlFor="exampleInputEmail1">Name</label>
                    <input type="text" onChange={ (e) => onChangeForm(e) } className="form-control" name="name" id="firstname" aria-describedby="emailHelp" placeholder="Name" />
                </div>
                <div className="form-group col-md-4">
                    <label htmlFor="exampleInputPassword1">Email</label>
                    <input type="text" onChange={ (e) => onChangeForm(e) } className="form-control" name="email" id="lastname" aria-describedby="emailHelp" placeholder="Email" />                    
                </div>
                <div className="form-group col-md-4">
                    <label htmlFor="exampleInputPassword1">Password</label>
                    <input type="text" onChange={ (e) => onChangeForm(e) } className="form-control" name="password" id="password" aria-describedby="emailHelp" placeholder="Password" />                    
                </div>
            </div>
            <div className="row">
                <div className="form-group col-md-4">
                    <label htmlFor="exampleInputAddress1">Address</label>
                    <input type="text" onChange={ (e) => onChangeForm(e) } className="form-control" name="address" id="address" aria-describedby="addressHelp" placeholder="Address" />                    
                </div>
                <div className="form-group col-md-4">
                    <label htmlFor="exampleInputSex1">Sex</label>
                    <input type="text" onChange={ (e) => onChangeForm(e) } className="form-control" name="sex" id="Sex" aria-describedby="SexHelp" placeholder="Sex" />                    
                </div>
                <div className="form-group col-md-4">
                    <label htmlFor="exampleInputPhone1">Phone</label>
                    <input type="tel" onChange={ (e) => onChangeForm(e) } className="form-control" name="phone" id="phone" aria-describedby="phoneHelp" placeholder="Phone" />                    
                </div>
            </div>
            <button type="button" onClick={(e) => createUser() } className="btn btn-danger">Create</button>
        </div>
    )
}

export default CreateUser;