import React from 'react';
import Content from './content'

export const DisplayBoard = ({ numberOfUsers, getAllUsers, setNewNumber, data }) => {
    return(
        <div className="display-board">
            <h4>Users Created</h4>
            <div className="number">
                {numberOfUsers}
            </div>
            <div className="btn">
                <button type="button" onClick={(e) => getAllUsers()} className="btn btn-warning">Get all Users</button>
            </div>
            <div>
                <button onClick={setNewNumber} className="btn btn-info">INCREMENT</button>
                <Content myNumber={data}></Content>
            </div>
        </div>
    )
}

export default DisplayBoard;