import React from 'react';
import {Button, Col, Container, Form, Modal, Row} from 'react-bootstrap';

export const EditUsers = ({show, editValue, handleClose}) => {
    
    return(
        <div>
            <Modal show={show} onHide={() => handleClose()}>
                <Modal.Header closeButton>
                    <Modal.Title>Edit User</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <Container>
                        <Row>
                            <Col md={4} className="form-group">
                                <Form.Label>Name</Form.Label>
                                <Form.Control type="text" name="name" value={editValue.name} />
                            </Col>
                            <Col md={4} className="form-group">
                                <Form.Label>Email</Form.Label>
                                <Form.Control type="email" name="email"  value={editValue.email} />
                            </Col>
                            <Col md={4} className="form-group">
                                <Form.Label>New Password</Form.Label>
                                <Form.Control type="password" name="password" placeholder="New Password"/>
                            </Col>
                        </Row>
                        <Row>
                            <Col md={4} className="form-group">
                                <Form.Label>Address</Form.Label>
                                <Form.Control type="text" name="address"  value={editValue.address} />
                            </Col>
                            <Col md={4} className="form-group">
                                <Form.Label>Phone</Form.Label>
                                <Form.Control type="tel" name="phone"  value={editValue.phone} />
                            </Col>
                            <Col md={4} className="form-group">
                                <Form.Label>Sex</Form.Label>
                                <Form.Control type="text" name="sex"  value={editValue.sex} />
                            </Col>
                        </Row>
                    </Container>
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="primary" onClick={() => handleClose()}>
                        Update
                    </Button>
                </Modal.Footer>
            </Modal>
        </div>
        
    )
}

export default EditUsers;