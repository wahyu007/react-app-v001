import React from 'react';

export const Users = ( {users, onSearchForm, getSearch, getUser} ) => {
    if(users.length === 0) return null

    const UserRow = (user, index) => {
        return(
            <tr key={ index } className={index%2 === 0? 'odd':'even'}>
                <td>{index + 1}</td>
                <td>{user.name}</td>
                <td>{user.email}</td>
                <td>{user.address}</td>
                <td>{user.sex}</td>
                <td>{user.phone}</td>
                <td>
                    <button className="btn btn-info" onClick={() => getUser(users[index])}>Update</button>
                    <button className="btn btn-warning">Delete</button>
                </td>
            </tr>
        )
    }

    const userTable = users.map((user, index) => UserRow(user, index))

    return(
        <div className="container">
            <h2>Users</h2>
            <div className="row">
                <div className="col-md-4 offset-8">
                    <div className="input-group">
                        <input className="form-control" onChange={ (e) => onSearchForm(e) } type="text" name="search" placeholder="Search Name"
                        aria-label="Search" />
                        <div className="input-group-append">
                            <button className="btn btn-outline-primary" onClick={() => getSearch()}>Search</button>
                        </div>
                    </div>
                </div>
            </div>
            <table className="table table-bordered">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Address</th>
                        <th>Sex</th>
                        <th>Phone</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    {userTable}
                </tbody>
            </table>
        </div>
    )
}

export default Users;