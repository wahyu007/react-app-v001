import React, { Component } from 'react';
import './bootstrap.min.css';
import './App.css';
import { Header } from './components/Header';
import { Users } from './components/Users';
import { EditUsers } from './components/EditUsers';
import { DisplayBoard } from './components/DisplayBoard'
import CreateUser from './components/CreateUser'
import { getAllUsers, createUser, getSearchUsers} from './services/UserService';

class App extends Component {
  state = {
    user : {},
    users: [],
    numberOfUsers: 0,
    search:'',
    show: false,
    editValue: {},
    data: 0
    // setNewNumber = this.setNewNumber.bind(this)
  }

  setNewNumber = () => {
    this.setState({ data: this.state.data + 1})
  }

  handleClose = () => {
    this.setState({ show: false})
  };

  createUsers = (e) => {
    console.log(`create user ${this.state.user}`)
    createUser(this.state.user)
      .then(response => {
        console.log(response);
        this.setState({ numberOfUsers: this.state.numberOfUsers + 1})
      });
  }

  getAllUsers = () => {
    getAllUsers()
    .then(users => {
      console.log(users)
      this.setState({ users: users, numberOfUsers: users.length})
    })
  }

  getUser = (user) => {
    this.setState({ show: true, editValue: user});
    console.log(user);
  }

  getSearch = () => {
    if(this.state.search === ''){
      getAllUsers();
    } else {
      getSearchUsers(this.state.search)
      .then(users => {
        if(users.length <= 0) {
          this.setState({ users: [{name: "null"}]})
        } else {
          this.setState({ users: users})
        }
      })
    }
  }

  onChangeForm = (e) => {
    let user = this.state.user;
    if(e.target.name === 'name' ){
      user.name = e.target.value
    } else if ( e.target.name === 'email' ){
      user.email = e.target.value;
    } else if(e.target.name === 'password'){
      user.password = e.target.value
    } else if(e.target.name === 'sex'){
      user.sex = e.target.value
    } else if(e.target.name === 'address'){
      user.address = e.target.value
    } else if(e.target.name === 'phone'){
      user.phone = e.target.value
    }

    console.log(user)
    this.setState({user})
  }

  onSearchForm = (e) => {
    console.log(e.target.value);
    this.setState({search: e.target.value});
  }

  render() {
    return(
      <div className="App">
        <Header></Header>
        <div className="container mrgnbtm">
          <div className="row">
            <div className="col-md-8">
              <CreateUser
                user={this.state.user}
                onChangeForm={this.onChangeForm}
                createUser = {this.createUsers}
              ></CreateUser>
            </div>
            <div className="col-md-4">
              <DisplayBoard
                data={this.state.data}
                setNewNumber={this.setNewNumber}
                numberOfUsers={this.state.numberOfUsers}
                getAllUsers={this.getAllUsers}
              ></DisplayBoard>
            </div>
          </div>
          <div className="row mrgnbtm">
            <Users 
              users={this.state.users}
              onSearchForm={this.onSearchForm} 
              getSearch={this.getSearch}
              getUser={this.getUser}
              >
            </Users>
          </div>
        </div>
        <EditUsers
          show={this.state.show}
          editValue={this.state.editValue}
          handleClose={this.handleClose}
          >
        </EditUsers>
      </div>
    )
  }
}

export default App;